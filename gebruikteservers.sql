-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2017 at 10:48 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gebruikteservers`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `ContID` int(11) NOT NULL,
  `Email` varchar(45) NOT NULL,
  `Naam` varchar(200) NOT NULL,
  `Comment` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`ContID`, `Email`, `Naam`, `Comment`) VALUES
(1, 'Bart.graaf06@gmail.com', 'Bart de Graaf', 'OKE'),
(2, 'Bart.graaf06@gmail.com', 'Bart de Graaf', 'OKE'),
(3, 'Bart.graaf06@gmail.com', 'Bart de Graaf', 'ok'),
(4, 'Bart.graaf06@gmail.com', 'Bart de Graaf', 'ok'),
(5, 'Darkemail34@gmail.com', 'Bart de Graaf', 'OK\r\n'),
(6, 'Amdegraafjes@gmail.com', 'Martine', 'Hai'),
(7, 'Darkemail34@gmail.com', 'Bart de Graaf', 'OK\r\n'),
(8, 'Darkemail34@gmail.com', 'Bart de Graaf', 'OK\r\n'),
(9, '', '', ''),
(10, 'Bart.graaf06@gmail.com', 'Bart de Graaf', ''),
(11, 'Bart.graaf06@gmail.com', 'Bart de Graaf', ''),
(12, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '1rwefsfdsf'),
(13, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '1rwefsfdsf'),
(14, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(15, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(16, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(17, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(18, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(19, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(20, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(21, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(22, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(23, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(24, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(25, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(26, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(27, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(28, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(29, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(30, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(31, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(32, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(33, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe'),
(34, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '3qwrqqweqwe');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `ProductID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `Productname` varchar(45) NOT NULL,
  `ProductInfo` longtext NOT NULL,
  `Price` int(11) NOT NULL,
  `ProductStatus` int(11) NOT NULL,
  `Condition` int(11) NOT NULL,
  `Videokaart` varchar(300) NOT NULL,
  `Ram` varchar(300) NOT NULL,
  `Moederbord` varchar(300) NOT NULL,
  `Voeding` varchar(300) NOT NULL,
  `Processor` varchar(300) NOT NULL,
  `Pic_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ProductID`, `userID`, `Productname`, `ProductInfo`, `Price`, `ProductStatus`, `Condition`, `Videokaart`, `Ram`, `Moederbord`, `Voeding`, `Processor`, `Pic_ID`) VALUES
(22, 4, 'HPE ProLiant', 'Test', 1500, 0, 1, 'Nvidia', '64', 'Asrock', '1500w', 'i7', 1497447434);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `UserID` int(11) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `Rights` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserID`, `Email`, `Name`, `Password`, `Rights`) VALUES
(1, 'eftimov.daniel@gmail.com', 'Daniel', '$2y$10$BRs8NbOujV3HAffaU73WvedpZhIV67FnXaZdJRJ2IsAWL6ApDXXNS', 1),
(2, 'test@gmail.com', 'Test', '$2y$10$tErf9sWiIFoAUF3WbsYzuuqUo9dNXxNggzcVc0.LRNd3tGoz/jA2G', 0),
(3, 'Bramvankempen1999@gmail.com', 'Bram', '$2y$10$wdlbTSoX.O6u9eRCd85LSu3jql4iBShr/HhbRoZpPlCjgLrvW3eWq', 1),
(4, 'Bart.graaf06@gmail.com', 'Bart de Graaf', '$2y$10$HEOVu8TvSSHyIdrthJ7xVewuWUSs.7PySDUucyhwiXKB2BWJS.qFW', 1),
(5, 'Max@gmail.com', 'Max', '$2y$10$CI715H12fArueTdu0lZDzOvvvKkIYKiENqz1rbilHe3h42jLVtEMy', 0),
(6, 'henk@gmail.com', 'Henk de Koe', '$2y$10$D0aykDtjjzNxBg.f0zfq.eU1EUn70GinHLKb7hHyebJWPSFst/ZU6', 0),
(7, 'Martine@gmail.com', 'Martine', '$2y$10$baoZDWjId5sSsRX/eTWgFuCP8xQwDqMblXRy46yfLRNsuAs4og662', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`ContID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ProductID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UserID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `ContID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `ProductID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
