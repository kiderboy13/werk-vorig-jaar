<?php
	include_once("php/functions.php"); 
	$succes = false;
    if(checkSession() && $_SESSION["RIGHTS"] == 1 && isset($_POST['submit'])){
       if(register($conn, htmlspecialchars($_POST['email']), htmlspecialchars($_POST['name']), htmlspecialchars($_POST['pword']), $_POST['right'])){
		   header("Location: profile.php?p=registeren&m=user&s=succes");
	   }else{
		   header("Location: profile.php?p=registeren&m=user&s=failed");
	   }
    }else{
		header("Location: profile.php?p=registeren&m=user&s=failed");
	}
?>