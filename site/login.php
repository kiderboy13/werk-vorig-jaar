<?php
	include_once("php/functions.php");
	if(isset($_POST["submit"])){
		$email = htmlspecialchars($_POST["email"]);
		$password = htmlspecialchars($_POST["password"]);
		if(!login($conn, $email, $password)){
			header("Location: home.php?l=failed");
		}else{
			header("Location: home.php");
		}
	}else{
		header("Location: home.php");
	}
?>