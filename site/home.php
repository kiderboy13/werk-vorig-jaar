<?php
	include_once("php/functions.php");
?>

<html>
	<head>
        <title>Home</title>
		<?php getBasicHeadContent(); ?>
		<script>
            function pagedefine(){
                if(document.getElementById('paged').innerHTML == "&nbsp;Oud"){
                    document.getElementById('paged').innerHTML = "&nbsp;Nieuw";
                }else{
                    document.getElementById('paged').innerHTML = "&nbsp;Oud";
                }
            }
            
            function checkPrice(type){
                if(typeof type == "string"){
                    var min = document.form.minP;
                    var max = document.form.maxP;
                    if(type == "min"){
                        if(parseInt(min.value) > parseInt(max.value)){
                            min.value = max.value;
                        }
                    }else if(type == "max"){
                        if(parseInt(max.value) < parseInt(min.value)){
                            max.value = min.value;
                        }
                    }
                }
            }
            
            function scale(){
				if($(window).width() > 885){
					$("#servers").css("width", ($("#content").width() - $("#left-nav").width() - 20));
				}
			}
			
			window.addEventListener('resize', function(event){
				scale();
			});
		</script>
	</head>
	<body onload="scale()">
		<?php createMenu("home.php"); ?>
		<div class="container" id="content">
			<div id="left-nav" class="sidebar-nav">
				<form name="form" method="post" action="">
					Min price
					<input required class="black form-control" oninput="checkPrice('min');" name="minP" min="0" value="<?=((isset($_POST["minP"])) ? $_POST["minP"] : 0)?>" type="number">
					Max price
					<input required class="black form-control" oninput="checkPrice('max');" name="maxP" min="0" max="999999" value="<?=((isset($_POST["maxP"])) ? $_POST["maxP"] : 10000)?>" type="number">Status<br>
					<select name="stat" class="form-control">
						<option value=4 name="Good" <?=((isset($_POST["stat"]) && $_POST["stat"] == 4) ? "selected" : "")?>>Alles</option>
						<option value=0 name="Good" <?=((isset($_POST["stat"]) && $_POST["stat"] == 0) ? "selected" : "")?>>Goed</option>
						<option value=1 name="Good" <?=((isset($_POST["stat"]) && $_POST["stat"] == 1) ? "selected" : "")?>>Matig</option>
						<option value=2 name="Good" <?=((isset($_POST["stat"]) && $_POST["stat"] == 2) ? "selected" : "")?>>Slecht</option>
						<option value=3 name="Good" <?=((isset($_POST["stat"]) && $_POST["stat"] == 3) ? "selected" : "")?>>Defect</option>
					</select><br>
					<button class="btn btn-default" name="submitP">Zoek</button>
                    
					<label class="switch">
					<?php if(!isset($_POST["submitP"])){$_POST["con"] = "";} ?>
						<input id="con" type="checkbox" onchange="pagedefine()" name="con" <?=((isset($_POST["con"])) ? "checked" : "")?>>
						<div class="slider round"></div>
					</label>
                    <span id="paged">&nbsp;<?=((isset($_POST["con"])) ? "Nieuw" : "Oud")?></span>
				</form>
			</div>
            <div class="container servercontainer" id="servers">
				<?php if(isset($_POST['submitP'])){
						$minP = $_POST['minP'];
						$maxP = $_POST['maxP'];
						$stat = $_POST['stat'];
						if($stat == 4){
							$query = "SELECT * FROM `products` WHERE `Price` BETWEEN $minP AND $maxP AND `Condition` = " . ((isset($_POST["con"])) ? "1" : "0");
						} else{
							$query = "SELECT * FROM `products` WHERE `Price` BETWEEN $minP AND $maxP AND `ProductStatus` = $stat AND `Condition` = " . ((isset($_POST["con"])) ? "1" : "0");
						}  
                    }else{     
                        $query = "SELECT * FROM `products` WHERE`Condition` = " . ((isset($_POST["con"])) ? "1" : "0");
                    }
					$results = mysqli_query($conn, $query);
					$color = ["one", "two", "three"];
					$counter = 0;
					while($row = mysqli_fetch_assoc($results)){ ?>
						<div class="server <?=$color[$counter]?> open" onclick="infoToModal(this);" data-toggle="modal" data-target="#myModal">
                            <img src="img/products/<?=$row["Pic_ID"]?>.png" width="256" height="144">
							<label>&nbsp;Productnaam: <?=$row["Productname"]?></label><br>
							<span>&nbsp;Processor: <?=$row["Processor"]?></span><br>
							<span>&nbsp;Videokaart: <?=$row["Videokaart"]?></span><br>
							<span>&nbsp;Moederbord: <?=$row["Moederbord"]?></span><br>
							<span>&nbsp;Ram: <?=$row["Ram"]?></span><br>
							<span>&nbsp;Voeding: <?=$row["Voeding"]?></span><br>
							<span>&nbsp;Prijs: €<?=$row["Price"]?></span>
							<span class="infotab"><br>&nbsp;<b>Extra informatie:</b><br><div class="info"><?=$row["ProductInfo"]?></div></span>
						</div>
					<?php
						$counter++;
						if($counter > 2){
							$counter = 0;
						}
                } ?>
				</div>
		</div>
		<?php
			createModal();
			createFooter($conn); 
		?>
	</body>
</html>