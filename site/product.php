<?php
	include_once("php/functions.php");        
    if(checkSession() && $_SESSION["RIGHTS"] == 1 && isset($_POST['submit'])){
        $specs = $_POST['xinfo'];
		if(!empty($_FILES["pic"]["name"])){
			$picID = time();
			if(uploadPicture($picID)){
				if(createProduct($conn, $_POST['uID'], $_POST['namep'], $specs, $_POST['price'], $_POST['stat'], $_POST['cond'],$_POST['vidk'],$_POST['ram'],$_POST['mother'],$_POST['psu'],$_POST['proc'], $picID)){
					header("Location: profile.php?p=product&m=product&s=succes");
				}else{
					unlink("img/products" . $picID . ".png");
					header("Location: profile.php?p=product&m=product&s=failed");
				}
			}else{
				header("Location: profile.php?p=product&m=product&s=failed");
			}
		}else{
			if(createProduct($conn, $_POST['uID'], $_POST['namep'], $specs, $_POST['price'], $_POST['stat'], $_POST['cond'],$_POST['vidk'],$_POST['ram'],$_POST['mother'],$_POST['psu'],$_POST['proc'], 0)){
				header("Location: profile.php?p=product&m=product&s=succes");
			}else{
				header("Location: profile.php?p=product&m=product&s=failed");
			}
		}
    }else{
		header("Location: profile.php?p=product&m=product&s=failed");
	}
?>