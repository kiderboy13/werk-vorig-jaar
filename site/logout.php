<?php
	include_once("php/functions.php");
	unset($_COOKIE['PHPSESSID']);
    setcookie('PHPSESSID', null, -1, '/');
	$_SESSION = array();
	header("Location: home.php");
?>