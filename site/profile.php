<?php
	include_once("php/functions.php");
	if(!checkSession()){
		header("Location: home.php");
	}
	$page = ((isset($_GET["p"])) ? $_GET["p"] : "producten");
	$switch;
	if($_SESSION["RIGHTS"] == 0){
		if($page == "registeren" || $page == "product" || $page == "berichten"){
			header("Location: profile.php");
		}
	}
	if(isset($_POST["submit"]) && $_POST["submit"] == "status"){
		$sql = "UPDATE `products` SET `ProductStatus` = " . $_POST["statc"] . " WHERE `ProductID` = " . $_POST["pID"];
		mysqli_query($conn, $sql);
		$sql = "UPDATE `products` SET `ProductStatus2` = " . $_POST["statc2"] . " WHERE `ProductID` = " . $_POST["pID"];
		mysqli_query($conn, $sql);
	}
?>

<!DOCTYPE html>
<html lang="nl">
	<head>
        <title>Profiel</title>
		<?php getBasicHeadContent(); ?>
		<link rel="stylesheet" href="css/profile.css?v=<?=time()?>">
		<style>
			#filter{
				<?php if($_SESSION["RIGHTS"] == 1){ ?>
					top: 287px;
				<?php }else{ ?>
					top: 167px;
				<?php } ?>
			}
		</style>
		<script>
            function pagedefine(){
                if(document.getElementById('paged').innerHTML == "&nbsp;Oud"){
                    document.getElementById('paged').innerHTML = "&nbsp;Nieuw";
                }else{
                    document.getElementById('paged').innerHTML = "&nbsp;Oud";
                }
            }
            
			function inputFilled(){
				var a = document.getElementById("email").value;
				var b = document.getElementById("name").value;
				var c = document.getElementById("npword").value;
				var d = a + b + c;
				if(d != "" && d.replace(/\s/g, '').length){
					return true;
				}
				alert("Er moet een veld ingevult worden");
				return false;
			}
			
			<?php if($_SESSION["RIGHTS"] == 1){ ?>
				function validate(){
					var a = document.getElementById("pword").value;
					var b = document.getElementById("pword2").value;
					if (a!=b) {
					   document.getElementById("pword2").style.border = '1px solid red';
					   document.getElementById("pword2").style.color = 'red';
					   document.getElementById("err").style.color='red';
					   document.getElementById("err").style.display='block'; 
					   return false;
					}  
				}
			<?php } ?>
			
			<?php switch($page){
					case "producten": ?>
						function loadFilter(){
							document.getElementsByClassName("modal-title")[0].innerHTML = "";
							var body = document.getElementsByClassName("modal-body")[0];
							body.innerHTML = document.getElementById("filter").innerHTML;
						}
						<?php break;
					case "berichten": ?>
						function messageOverflow(){
							var comments = document.getElementsByClassName("comment");
							for(var i = 0; i < comments.length; i++){
								if(comments[i].offsetHeight < comments[i].scrollHeight){
									var overflow = comments[i].getElementsByTagName("div")[0];
									overflow.style.display = "block";
								}
							}
						}
						
						function loadMessage(message){
							document.getElementsByClassName("modal-title")[0].innerHTML = message.getElementsByClassName("info")[0].innerHTML;
							var body = document.getElementsByClassName("modal-body")[0];
							body.innerHTML = message.innerHTML;
							body.getElementsByClassName("info")[0].style.display = "none";
							body.getElementsByClassName("comment")[0].getElementsByClassName("overflow")[0].style.display = "none";
						}
			<?php } ?>
			
			function scale(){
				$("#pcontent").css("width", ($("#content").width() - $("#options").width()));
			}
			
			window.addEventListener('resize', function(event){
				scale();
			});
		</script>
	</head>
	<body onload="scale(); <?php if($page == "registeren"){ ?>document.getElementById('err').style.display='none';<?php }elseif($page == "berichten"){ ?>messageOverflow(); <?php } ?>">
		<?php createMenu("profile.php") ?>
		<div class="container" id="content">
			<?php
				if(isset($_GET["m"]) && isset($_GET["s"])){
					$color = (($_GET["s"] == "succes") ? "#718b30" : "#7f413f");
					$bcolor = (($_GET["s"] == "succes") ? "#5a6e30" : "rgb(186, 81, 75)"); ?>
					<div style="background-color: <?=$color?>; color: #fff; border: 3px solid <?=$bcolor?>">
						<?php
							switch($_GET["m"]){
								case "product": echo (($_GET["s"] == "succes") ? "Product is toegevoegd!" : "Er was een fout bij het toevoegen van het product"); break;
								case "user": echo (($_GET["s"] == "succes") ? "Gebruiker is geregistreerd!" : "Gebruiker kon niet geregistreerd worden"); break;
								case "edit": echo (($_GET["s"] == "succes") ? "Gegevens zijn gewijzigd!" : "Gegevens kunnen niet worden gewijzigd"); break;
							}
						?>
					</div>
					<?php
				}
			?>
			<div class="sidebar-nav" id="options">
				<li><a href="<?=$_SERVER["PHP_SELF"] . "?p=producten"?>" <?=(($page == "producten") ? 'class="active"' : '')?>><span class="glyphicon glyphicon-th-list"></span> <span class="text">Producten</span></a></li>
				<li><a href="<?=$_SERVER["PHP_SELF"] . "?p=edit"?>" <?=(($page == "edit") ? 'class="active"' : '')?>><span class="glyphicon glyphicon-pencil"></span> <span class="text">Gegevens wijzigen</span></a></li>
				<?php if($_SESSION["RIGHTS"] == 1){ ?>
					<li><a href="<?=$_SERVER["PHP_SELF"] . "?p=registeren"?>" <?=(($page == "registeren") ? 'class="active"' : '')?>><span class="glyphicon glyphicon-user"></span> <span class="text">Gebruiker registreren</span></a></li>
					<li><a href="<?=$_SERVER["PHP_SELF"] . "?p=product"?>" <?=(($page == "product") ? 'class="active"' : '')?>><span class="glyphicon glyphicon-plus"></span> <span class="text">Product toevoegen</span></a></li>
                    <li><a href="<?=$_SERVER["PHP_SELF"] . "?p=berichten"?>" <?=(($page == "berichten") ? 'class="active"' : '')?>><span class="glyphicon glyphicon-envelope"></span> <span class="text">Berichten</span></a></li>
				<?php }
					if($page == "producten"){ ?>
						<li id="filter_button"><a onclick="loadFilter();" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-filter"></span></a></li>
				<?php } ?>
			</div>
            <?php if($page == "producten"){ ?>
			<div id="filter">
				<form name="filterForm" method="post" action="">
					Min price
					<input class="black form-control" name="minP" min="0" value="<?=((isset($_POST["minP"])) ? $_POST["minP"] : 0)?>" type="number" required>
					Max price
					<input class="black form-control" name="maxP" min="0" value="<?=((isset($_POST["maxP"])) ? $_POST["maxP"] : 10000)?>" type="number" required>Status<br>
					<select name="stat" class="form-control">
						<option value=4 name="Good" <?=((isset($_POST["stat"]) && $_POST["stat"] == 4) ? "selected" : "")?>>Alles</option>
						<option value=0 name="Good" <?=((isset($_POST["stat"]) && $_POST["stat"] == 0) ? "selected" : "")?>>Goed</option>
						<option value=1 name="Good" <?=((isset($_POST["stat"]) && $_POST["stat"] == 1) ? "selected" : "")?>>Matig</option>
						<option value=2 name="Good" <?=((isset($_POST["stat"]) && $_POST["stat"] == 2) ? "selected" : "")?>>Slecht</option>
						<option value=3 name="Good" <?=((isset($_POST["stat"]) && $_POST["stat"] == 3) ? "selected" : "")?>>Defect</option>
					</select>
                    <?php if($_SESSION["RIGHTS"] == 1){?>
                        Gebruiker
                    <select name="uID" class="form-control">
						<?php
							$result = mysqli_query($conn, $SQL = "SELECT `userID` , `Email` FROM users ORDER BY `Email`");
							$uID = isset($_POST["uID"]);
							echo "<option selected value=''>Alle</option>";
							while($mail = mysqli_fetch_assoc($result)){
								if($uID){
									if($_POST["uID"] == $mail["userID"]){
										echo '<option selected value="' . $mail['userID'] . '">' . $mail['Email'] . '</option>';
										$uID = true;
									}else{
										echo '<option value="' . $mail['userID'] . '">' . $mail['Email'] . '</option>';
									}
								}elseif($_SESSION["USERID"] == $mail["userID"]){
									echo '<option selected value="' . $mail['userID'] . '">' . $mail['Email'] . '</option>';
								}else{
									echo '<option value="' . $mail['userID'] . '">' . $mail['Email'] . '</option>';
								}
							}
						?><?php } ?>
				    </select><br>
					<button class="btn btn-default" name="submitP">Zoek</button>
					<label class="switch">
						<?php 
							if(!isset($_POST["submitP"])){$switch = "1";}else{$switch = ((isset($_POST["con"])) ? "1" : "0");}
						?>
						<input id="con" type="checkbox" onchange="pagedefine()" name="con" <?=(($switch == "1") ? "checked" : "")?>>
						<div class="slider round"></div>
					</label>
                    <span id="paged">&nbsp;<?=(($switch == "1") ? "Nieuw" : "Oud")?></span>
				</form>
			</div><?php }  ?>
			<div id="pcontent">
				<?php switch($page){
					case "producten": ?>
						<div id="products">
							<?php
								if(isset($_POST['submitP'])){
									$minP = $_POST['minP'];
									$maxP = $_POST['maxP'];
									$stat = $_POST['stat'];
									if($stat == 4){
										$query = "SELECT * FROM `products` WHERE `Price` BETWEEN $minP AND $maxP AND `Condition` = " . $switch . ((isset($_POST["uID"]) && $_POST["uID"] != '') ? " AND `UserID` = " . $_POST["uID"] : (($_SESSION["RIGHTS"] == 0) ? " AND `UserID` = " . $_SESSION["USERID"] : ""));
									} else{
										$query = "SELECT * FROM `products` WHERE `Price` BETWEEN $minP AND $maxP AND `ProductStatus` = $stat AND `Condition` = " . $switch . ((isset($_POST["uID"]) && $_POST["uID"] != '') ? " AND `UserID` = " . $_POST["uID"] : (($_SESSION["RIGHTS"] == 0) ? " AND `UserID` = " . $_SESSION["USERID"] : ""));
									}  
								}else{     
									$query = "SELECT * FROM `products` WHERE `UserID` = " . $_SESSION["USERID"] . " AND `condition` = 1 ";
								}
								$results = mysqli_query($conn, $query);
								$productAmount = mysqli_num_rows($results);
								$color = ["one", "two", "three"];
								$counter = 0;
								if(mysqli_num_rows($results) > 0){
									while($row = mysqli_fetch_assoc($results)){ ?>
										<form name="sform" method="post" class="server <?=$color[$counter]?> open" onclick="infoToModal(this);" data-toggle="modal" data-target="#myModal">
											<?php $pid = $row['ProductID']?>
											<a id="del" style="" href="delete.php?messageid= <?=$pid?>" class="glyphicon glyphicon-remove" value=""></a>
											<img src="img/products/<?=$row["Pic_ID"]?>.png" width="250" height="140">
											<label>&nbsp;Productnaam: <?=$row["Productname"]?></label><br>
											
											
											<span>&nbsp;<b>Processor:</b> <?=$row["Processor"]?></span><br>
											<span>&nbsp;<b>Videokaart:</b> <?=$row["Videokaart"]?></span><br>
											<span>&nbsp;<b>Moederbord:</b> <?=$row["Moederbord"]?></span><br>
											<span>&nbsp;<b>Ram:</b> <?=$row["Ram"]?></span><br>
											<span>&nbsp;<b>Voeding:</b> <?=$row["Voeding"]?></span><br>
											<span>&nbsp;<b>Prijs:</b> €<?=$row["Price"]?></span><br>
											<span class="infotab"><br>&nbsp;<b>Extra informatie:</b><br><div class="info"><?=$row["ProductInfo"]?></div></span>
											<?php if($_SESSION["RIGHTS"] == 1){ ?>
												<select id="statc" name='statc' onclick="event.stopPropagation();" oninput="document.getElementById('statSubmit').click();" class="form-control">
													<option <?=(($row['ProductStatus'] == 0) ? "selected" : "")?> value="0">Goed</option>
													<option <?=(($row['ProductStatus'] == 1) ? "selected" : "")?> value="1">Matig</option>
													<option <?=(($row['ProductStatus'] == 2) ? "selected" : "")?> value="2">Slecht</option>
													<option <?=(($row['ProductStatus'] == 3) ? "selected" : "")?> value="3">Defect</option>
												</select><br><br>
												<select id='statc2' name='statc2' onclick="event.stopPropagation();" oninput="document.getElementById('statSubmit').click();"  class="form-control">
													<option <?=(($row['ProductStatus2'] == 0) ? "selected" : "")?> value="0">Binnengekomen</option>
													<option <?=(($row['ProductStatus2'] == 1) ? "selected" : "")?> value="1">Gewist</option>
													<option <?=(($row['ProductStatus2'] == 2) ? "selected" : "")?> value="2">Vernietigd</option>
													<option <?=(($row['ProductStatus2'] == 3) ? "selected" : "")?> value="3">Verkocht/in de verkoop</option>
												</select>
												<input type="hidden" name="pID" value="<?=$row["ProductID"]?>">
												<?php if(isset($_POST["minP"])){ ?>
													<input type="hidden" name="minP" value="<?=$_POST["minP"]?>">
													<input type="hidden" name="maxP" value="<?=$_POST["maxP"]?>">
													<input type="hidden" name="stat" value="<?=$_POST["stat"]?>">
													<input type="hidden" name="submitP">
												<?php }if(isset($_POST["con"])){ ?>
													<input type="hidden" name="con" value="<?=$_POST["con"]?>">
												<?php }if(isset($_POST["uID"])){ ?>
													<input type="hidden" name="uID" value="<?=$_POST["uID"]?>">
												<?php } ?>
												<input type="submit" name="submit" value="status" style="display: none;" id="statSubmit">
											<?php }else{
													$status2;
													if($row["ProductStatus2"] == 1){
														$status2 = "Mee bezig";
													}else{
														$status2 = "Afgehandeld";
													}
													echo "<br>&nbsp;Product status: " . $status2;
												} ?>
										</form>
									<?php
										$counter++;
										if($counter > 2){
											$counter = 0;
										}
									}
								}else{
									echo '<div id="alert" style="">Geen producten gevonden</div>';
								}?>
						</div>
				<?php break; case "edit": ?>
						<div id="edit">
							<div style="width: 90%; display: block; margin: 0 auto;">
								<h1 class="white">Gegevens wijzigen</h1>
								<form onSubmit="return inputFilled()" action="edituser.php" method="post">
									<div class="form-group">
										<label class="white" for="email">Email:</label>
										<input class="form-control" placeholder="Email" id="email" type="email" name="email" value="<?=$_SESSION["USEREMAIL"]?>">
									</div>
										<div class="form-group">
										<label class="white" for="vnaam">Naam:</label>
										<input class="form-control" placeholder="Naam" id="name" type="text" name="name" value="<?=$_SESSION["USERNAME"]?>">
										</div>
									<div class="form-group">
										<label class="white" for="pword">Wachtwoord:</label>
										<input class="form-control" required placeholder="Wachtwoord" id="pword" type="password" name="pword">
									</div>
									<div class="form-group">
										<label class="white" for="npword">Nieuw wachtwoord:</label>
										<input class="form-control" placeholder="Nieuw wachtwoord" id="npword" type="password" name="npword">
									</div><br>
									<button type="submit" name="submit" class="btn btn-default" id="formbutton">Wijzig gegevens</button>
								</form>
							</div>
						</div>
				<?php break; case "registeren": ?>
						<div id="createuser">
							<div style="width: 90%; display: block; margin: 0 auto;">
								<h1 class="white">Maak gebruiker</h1>
								<form onSubmit="return validate()" action="createuser.php" method="post">
									<div class="form-group">
										<label class="white" for="email">Email:</label>
										<input class="form-control" required placeholder="Email" type="email" name="email">
									</div>
										<div class="form-group">
										<label class="white" for="vnaam">Naam:</label>
										<input class="form-control" required placeholder="Naam" type="text" name="name">
										</div>
									<div class="form-group">
										<label class="white" for="pword">Wachtwoord:</label>
										<input class="form-control" required placeholder="Wachtwoord" id="pword" type="password" name="pword">
									</div>
									<div class="form-group">
										<label class="white" for="pword2">Herhaal wachtwoord:</label>
										<input class="form-control" required placeholder="Herhaal wachtwoord" id="pword2" type="password" name="pword2">
									</div><span id="err">Wachtwoord moet overeen komen</span>   
										<label class="radio-inline white">
										<input checked type="radio" value="0" name="right">Gebruiker
									</label>
									<label class="radio-inline white">
										<input type="radio" value="1" name="right">Admin
									</label><br>
									<button type="submit" name="submit" class="btn btn-default" id="formbutton">Registeren</button>
								</form>
							</div>
						</div>
				<?php break; case "product": ?>
						<div id="createproduct">
							<div style="width: 90%; display: block; margin: 0 auto;">
								<h1 class="white">Maak product</h1>
								<form name="pform" action="product.php" method="post" enctype="multipart/form-data">
									<div class="form-group">
										<label class="white" for="email">Email eigenaar:</label>
										<select name='uID' class="form-control">
											<?php
												$result = mysqli_query($conn, $SQL = "SELECT `userID` , `Email` FROM users ORDER BY `Email`");
												while($mail = mysqli_fetch_assoc($result)){
													echo "<option  value=" . $mail['userID'] . ">" . $mail['Email'] . "</option>";
												}
											?>
										</select>
									</div>
									<div class="form-group">
										<label class="white" for="email">Naam product:</label>
										<input class="form-control" reguierd placeholder="Naam van het product" type="text" name="namep">
									</div>
									<label class="white" for="vnaam">Specificaties:</label>
									<div class="form-group">
										<input class="form-control" required placeholder="Processor" type="text" name="proc">
									</div>
									<div class="form-group">
										<input class="form-control" required placeholder="Videokaart" type="text" name="vidk">
									</div>
									<div class="form-group">
										<input class="form-control" required placeholder="Ram" type="text" name="ram">
									</div>
									<div class="form-group">
										<input class="form-control" required placeholder="Moederbord" type="text" name="mother">
									</div>
									<div class="form-group">
										<input class="form-control" required placeholder="Voeding" type="text" name="psu">
									</div>
                                    <div class="form-group">
										<textarea class="form-control" required placeholder="Extra info" type="text" name="xinfo" ></textarea>
									</div>
									<div class="form-group">
										<label class="white" for="email">Prijs:</label>
										<input class="form-control" reguierd placeholder="Prijs van het product" type="number" name="price">
									</div>
									<input type="file" name="pic" style="display: none;"><a class="btn btn-default" onclick="document.pform.pic.click();">Upload foto</a><br><br>
									<label class="radio-inline white">
										<input checked type="radio" value="1" name="cond">Nieuw
									</label>
									<label class="radio-inline white">
										<input type="radio" value="0" name="cond">Tweedehands
									</label><br><br>
								  
									<label class="radio-inline white">
										<input checked type="radio" value="0" name="stat">Goed
									</label>
									<label class="radio-inline white">
										<input type="radio" value="1" name="stat">Matig
									</label>
									<label class="radio-inline white">
										<input type="radio" value="2" name="stat">Slecht
									</label>
									<label class="radio-inline white">
										<input type="radio" value="3" name="stat">Defect
									</label><br><br>
									<button type="submit" name="submit" class="btn btn-default" id="formbutton">Toevoegen</button>
								</form>
							</div>
						</div>
				<?php
                       break; case "berichten": ?>
                    <div id="bericht" class="white">
                    <?php
                    $query = "SELECT * FROM contact ORDER BY `contID` DESC";
                    $results=mysqli_query($conn, $query);
                        
                                while ($message = mysqli_fetch_array($results)) {
                                $mid = $message['ContID'] ?>
                                    <div class="message open" onclick="loadMessage(this);" data-toggle="modal" data-target="#myModal">
                                        
										<div class="info">
                                            <a id="del2" style="" href="deletem.php?messageidm= <?=$mid?>" class="glyphicon glyphicon-remove" value=""></a>
											<span class="glyphicon glyphicon-user"></span>
											<span class="name">&nbsp;<?=$message["Naam"]?></span>
											<span class="email">&nbsp;&nbsp;<?=$message["Email"]?></span>
										</div>
										<div class="comment">
											<?=$message["Comment"]?>
											<div class="overflow">.................</div>
										</div>
									</div>
                                <?php } ?>

                                
                    <?php } ?></div>
			</div>
		</div>
		<?php 
			createModal();
			createFooter($conn);
		?>
	</body>
</html>