<?php
	include_once("php/functions.php");

    if(isset($_POST['formsubmit'])){
        $Email = htmlspecialchars($_POST['email']);
        $Name = htmlspecialchars($_POST['name']);
        $Content = htmlspecialchars($_POST['message']);
        createContact($conn, $Email, $Name, $Content);
    }
?>

<html>
	<head>
		<?php getBasicHeadContent(); ?>
		<link rel="stylesheet" href="css/contact.css?v=<?=time()?>">
		<link rel="stylesheet" href="css/footer.css?=<?=time()?>">
        <title>Contact</title>
        <script>
        function initMap() {
        var uluru = {lat: 52.1213993, lng: 5.4222325};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
	</head>
	<body>
        <?php createMenu("contact.php") ?>
        <div id="panelmap" class="col-sm-10 col-lg-10 col-centered">
				<div class="panel-default">
					<div id="ptitle" class="panel-heading">
						<h1><i class="icon-map-marker main-color"></i> Locatie</h1>
					</div>
                    <div id="map" class="panel-body">
                        <script async defer
                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBAvQ2wPVHkPUFtAhUld77lzk-8vcsqe8Y&callback=initMap">
                        </script>
                    </div>
            </div>
        </div>
    <div class="container">
        <div id="in" class="col-sm-3 col-lg-3 col-right">
				<div class="panel-default">
					<div id="ptitle" class="panel-heading">
						<h1><i class="icon-map-marker main-color"></i>Informatie</h1>
                    </div>
                    <div class="black" id="infod">
                        Neemt u gerust contact met ons op<br><br>

                        Adres:<br>
                        Myserverparts<br>
                        Klokhoek 16<br>
                        3833 GX leusden<br><br>

                        Telefoon: 033-4951703<br>
                        Mobiel: 06-20008021<br><br>

                        Kvk: 32142483<br>
                        BTW nummer: NL1345.52.283.B01<br>
                        Rabobank: 1477.20230<br>
                    </div>
                </div>
            </div>
        <div id="form" class="col-sm-7 col-lg-7 col-right2">
				<div class="panel-default">
					<div id="ptitle" class="panel-heading">
						<h1><i class="icon-map-marker main-color"></i>Contact</h1>
                    </div>
                    <div class="black" id="infod">
                        <form action="" method="post">
									<div class="form-group">
										<label class="Black" for="email">Email:</label>
										<input class="form-control" required placeholder="Email" type="email" name="email">
									</div>
										<div class="form-group">
										<label class="Black" for="vnaam">Naam:</label>
										<input class="form-control" required placeholder="Naam" type="text" name="name">
										</div>
									<div class="form-group">
										<label class="Black" for="pwd">Bericht:</label>
										<textarea class="form-control" rows="5" name="message" placeholder="Bericht"></textarea>
									</div>
									<button type="submit" name="formsubmit" class="btn btn-default" id="formbutton">Verzenden</button>
								</form>
							</div>
                    </div>
                </div>
            </div>
    </body>
    <?php 
        createModal();
		createFooter($conn);
    ?>
</html>