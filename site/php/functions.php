<?php
    function createProduct($conn, $userID, $productname, $productinfo, $price, $productstatus, $productcondition, $videokaart, $ram, $moederbord, $voeding, $processor, $picID){
        $SQL = "INSERT INTO products (`userID`, `Productname`, `ProductInfo`, `Price`, `ProductStatus`, `Condition`,`Videokaart`,`Ram`, `Moederbord`, `Voeding`,`Processor`, `Pic_ID`) VALUES ($userID, '$productname', '$productinfo', $price, $productstatus, $productcondition, '$videokaart', '$ram', '$moederbord', '$voeding', '$processor', $picID)";
		$result = mysqli_query($conn, $SQL);
		if($result !== false){
			return true;
		}else{
			return false;
		}
    }
	
	function uploadPicture($picID){
		if(!empty($_FILES["pic"]["name"])){
			$uploadPath = "img/products/" . basename($_FILES["pic"]["name"]);
			$pictureType = pathinfo($uploadPath, PATHINFO_EXTENSION);
			$pictureSize = getimagesize($_FILES["pic"]["tmp_name"]);
			if($pictureType != "png"){
				return false;
			}
			if(number_format($pictureSize[0] / $pictureSize[1], 1) != number_format(16 / 9, 1)){
				return false;
			}
			$uploadPath = "img/products/" . $picID . ".png";
			if(file_exists($uploadPath)){
				return false;
			}
			if(move_uploaded_file($_FILES["pic"]["tmp_name"], $uploadPath)){
				return true;
			}
			return false;
		}
	}

	function connectDB(){
		return mysqli_connect("localhost", "root", "", "gebruikteservers");
	}

	$conn = connectDB();
	if($conn !== false){
		session_start();
	}
	
	function checkSession(){
		$sessionID = session_id();
		if(isset($_SESSION["ID"]) && $_SESSION["ID"] == $sessionID){
			return true;
		}
		return false;
	}
	
	function login($conn, $email, $password){
		$result = mysqli_query($conn, "SELECT * FROM users");
		while($user = mysqli_fetch_assoc($result)){
			if($email == $user["Email"] && password_verify($password, $user["Password"])){
				$_SESSION["ID"] = session_id();
				$_SESSION["USEREMAIL"] = $user["Email"];
				$_SESSION["USERID"] = $user["UserID"];
				$_SESSION["USERNAME"] = $user["Name"];
				$_SESSION["RIGHTS"] = $user["Rights"];
				return true;
			}
		}
		return false;
	}
	
	function register($conn, $email, $Name, $password, $rights){
		$result = mysqli_query($conn, "SELECT * FROM users");
		while($user = mysqli_fetch_assoc($result)){
			if($email == $user["Email"]){
				return false;
			}
		}
		$encrypted_pass = password_hash($password, PASSWORD_DEFAULT);
		$SQL = "INSERT INTO users (UserID, Email, Name, Password, Rights) VALUES (NULL, '$email', '$Name', '$encrypted_pass', $rights)";
		mysqli_query($conn, $SQL);
		return true;
	}
	
	function getBasicHeadContent(){ ?>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/basic.css?v=<?=time()?>">
		<link rel="stylesheet" href="css/footer.css?v=<?=time()?>">
        <link rel="stylesheet" href="css/demo.css">
		<script src="js/jquery-3.2.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/product.js?v=<?=time()?>"></script>
	<?php }
	
	function createMenu($pageLink = ""){
		$pages = [["Home", "home.php"], ["Contact", "contact.php"]]; ?>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span> 
					</button>
					<a class="navbar-brand" href="home.php"><h3>Gebruikteservers<span>.nl</span></h3></a>
				</div>
				<div class="collapse navbar-collapse" id="nav">
					<ul class="nav navbar-nav">
						<?php
							foreach($pages as $page){ ?>
								<li <?php if($page[1] == $pageLink){echo 'class="active"';} ?>><a href="<?=$page[1]?>"><?=$page[0]?></a></li>
							<?php } ?>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<?php if(!checkSession()){ ?>
							<li><a data-toggle="collapse" data-target="#login"><span class="glyphicon glyphicon-log-in"></span> Inloggen</a></li>
						<?php }else{ ?>
							<li><a href="profile.php"><span class="glyphicon glyphicon-user"></span> <?=$_SESSION["USERNAME"]?></a></li>
							<li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Uitloggen</a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</nav>
		<?php $logincode = ((isset($_GET["l"])) ? $_GET["l"] : 0) ?>
		<div class="collapse <?php if($logincode === "failed"){echo "in";} ?> form-group" id="login">
			<?php
				if($logincode === "failed"){
					//echo '<script>alert("Email of wachtwoord verkeerd ingevuld");</script>';
				}
			?>
			<form action="login.php" method="post">
				<label>Email</label>
				<input class="form-control" type="email" name="email" required>
				<label>Wachtwoord</label>
				<input class="form-control" type="password" name="password" required>
				<button class="btn btn-<?=(($logincode === "failed") ? "danger" : "secondary")?>" type="submit" name="submit">Login</button>
			</form>
		</div>
	<?php }
    
    function createContact($conn, $Email, $Name, $Content){
        $SQL = "INSERT INTO contact (`Email`, `Naam`, `Comment`) VALUES ('$Email', '$Name', '$Content')";
		mysqli_query($conn, $SQL);
    }
	
	function createFooter($conn){
        if(isset($_POST['fsubmit'])){
            $Email = htmlspecialchars($_POST['email']);
            $Name = htmlspecialchars($_POST['name']);
            $Content = htmlspecialchars($_POST['message']);
            createContact($conn, $Email, $Name, $Content);
        }
        $year = new DateTime();
        ?>
		<footer class="footer-distributed">

			<div class="footer-left">

				<h3>Gebruikteservers<span>.nl</span></h3>

				<p class="footer-links">
					<a href="home.php">Home</a>
					·
					<a href="contact.php">Contact</a>
                    <?php if(checkSession()){
                    echo '· <a href="profile.php">Profiel</a>';}?>
				</p>

				<p class="footer-company-name">Gebruikteservers &copy; <?php echo $year->format('Y');?></p>

				<div class="footer-icons">
					<a href="https://www.facebook.com/bruno.windau"  target="_blank"><i class="fa fa-facebook"></i></a>
					<a href="https://twitter.com/bwindau" target="_blank"><i class="fa fa-twitter"></i></a>
					<a href="https://www.linkedin.com/in/bruno-windau-713a789" target="_blank"><i class="fa fa-linkedin"></i></a>
				</div>

			</div>
			<div class="footer-right">
				<p>Neem contact op</p>
				<form action="#" method="post">
					<input type="email" name="email" placeholder="Email" required/>
                    <input type="text" name="name" placeholder="Naam" required />
					<textarea name="message" placeholder="Bericht" required></textarea>
					<button name="fsubmit">Verzenden</button>

				</form>

			</div>

		</footer>
	<?php }
	
	function createModal(){ ?>
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">

			  <div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Modal Header</h4>
					</div>
					<div class="modal-body">
						<p>Some text in the modal.</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			  
			</div>
		</div>
	<?php }
?>