var screens;
var width = 400;
var height = 400;
var tileSize = 24;
var snake;
var food = generatePos();

function generatePos(){
	var x = Math.floor(Math.random() * (width / tileSize));
	var y = Math.floor(Math.random() * (height / tileSize));
	return new vector(x, y);
}

function vector(x, y){
	this.x = x;
	this.y = y;
	this.compare = function(vector){
		if(this.x == vector.x && this.y == vector.y){
			return true;
		}
		return false;
	}
}

function Snake(x = 0, y = 0){
	this.pos = new vector(x, y);
	this.dir = 5;
	this.points = 0;
	this.tail = [];
	this.eat = function(foodPos){
		if(this.pos.compare(foodPos)){
			this.points++;
			food = generatePos();
			return true;
		}
		return false;
	}
	this.update = function(){
		if(this.points > 0){
			this.tail[this.points - 1] = new vector(this.pos.x, this.pos.y);
		}
		for(i = 0; i < this.points - 1; i++){
			this.tail[i] = this.tail[i + 1];
		}
		switch(this.dir){
			case 0: this.pos.y--; break;
			case 1: this.pos.x++; break;
			case 2: this.pos.y++; break;
			case 3: this.pos.x--; break;
		}
		if(this.pos.x > Math.floor(width / tileSize)){
			this.pos.x = 0;
		}else if(this.pos.x < 0){
			this.pos.x = Math.floor(width / tileSize);
		}else if(this.pos.y > Math.floor(height / tileSize)){
			this.pos.y = 0;
		}else if(this.pos.y < 0){
			this.pos.y = Math.floor(height / tileSize);
		}
	}
}

function start(){
	screens = document.getElementsByClassName("snake-window");
	for(var i = 0; i < screens.length; i++){
		screens[i].width = width;
		screens[i].height = height;
	}
	snake = new Snake();
	setInterval(update, 1000 / 10);
	setInterval(render, 1000 / 30);
	document.addEventListener('keydown', function(event) {
		if(event.keyCode == 87 && snake.dir != 2) {
			snake.dir = 0;
		}else if(event.keyCode == 83 && snake.dir != 0) {
			snake.dir = 2;
		}else if(event.keyCode == 65 && snake.dir != 1) {
			snake.dir = 3;
		}else if(event.keyCode == 68 && snake.dir != 3) {
			snake.dir = 1;
		}
	});
}

function update(){
	snake.eat(food);
	snake.update();
}

function render(){
	for(var i = 0; i < screens.length; i++){
		var screen = screens[i].getContext("2d");
		screen.fillStyle = "#000";
		screen.fillRect(0, 0, width, height);
		screen.fillStyle = "#f00";
		screen.fillRect(food.x * tileSize, food.y * tileSize, tileSize, tileSize);
		screen.fillStyle = "#fff";
		for(j = 0; j < snake.points; j++){
			screen.fillRect(snake.tail[j].x * tileSize, snake.tail[j].y * tileSize, tileSize, tileSize);
		}
		screen.fillRect(snake.pos.x * tileSize, snake.pos.y * tileSize, tileSize, tileSize);
	}
}