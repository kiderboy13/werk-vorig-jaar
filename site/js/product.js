function infoToModal(product){
	var title = document.getElementsByClassName("modal-title")[0];
	var newtitle = product.getElementsByTagName("label")[0];
	title.innerHTML = newtitle.innerHTML.substring(13);
	var info = document.getElementsByClassName("modal-body")[0];
	var newinfo = product.getElementsByTagName("span");
	info.innerHTML = "";
	for(var i = 0; i < newinfo.length; i++){
		info.innerHTML += newinfo[i].innerHTML + "<br>";
	}
}