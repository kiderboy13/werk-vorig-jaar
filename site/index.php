<?php
	include_once("php/functions.php");
?>

<html>
	<head>
		<?php getBasicHeadContent(); ?>
        <link rel="stylesheet" href="css/land.css?v=<?=time()?>">
		<link rel="stylesheet" href="css/footer.css?=<?=time()?>">
        <title>Welkom</title>
	</head>
	<body>
        <div>
            <div class="white" id="cont">Welkom op <h3>gebruikteservers<span>.nl</span></h3></div><br>
            <div class="white" id="wel"><span id="landt">Welkom</span><br>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
            <div class="white" id="wel2"><span id="landt">Waarom gebruikte servers?</span><br>  
                    Snelle levering! <br>
                    Eigen keuze server! <br>
                    Lage prijzen! <br>
                    Ruim aanbod! <br>
                    Specificaties naar eigen wens! <br>
            </div>
                <div class="wrapper">
                    <button onclick="location.href = 'home.php'" id="landbu" class="btn btn-primary btn-lg">Kijk direct onze producten!</button>
                </div>
           </div>
	</body>
</html>